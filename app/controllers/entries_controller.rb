class EntriesController < ApplicationController
  def new
    @entry = Entry.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @entry }
    end
  end

  # POST /entries
  # POST /entries.json
  def create
    @entry = Entry.new(params[:entry])


    respond_to do |format|
      if @entry.save
        #format.html { redirect_to @entry, notice: 'Entry was successfully created.' }
        #format.json { render json: @entry, status: :created, location: @entry }
      else
        format.html { render action: "new" }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end
end
