var landing = {
    emailSend:function (form) {
        var email = $(form).find('#entry_email').val();
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var isValid =  re.test(email);
        if (isValid) {
            $('#changeable_withform').hide();
            $('#changeable_thanks').show();
            var formData = $(form).serialize();
            $.ajax({type:"POST", url:"entries/", data:formData});
        } else {
            $('#changeable_withform').show();
            $('#changeable_thanks').hide();
            $('.email_field').css({backgroundColor:'red'});
            $('.email_field').keydown(function(event) {
                $(event.target).css({backgroundColor:'white'});
            });
        }
        return false;
    }
}
