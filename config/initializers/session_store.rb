# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_ecollector_old_session',
  :secret      => '4316922a66f425f1f091c1f8cfaff16122779134777a7db9e596908b0dc03fc1d421f4aa0b5f6ca90ef804f8c435975f79ec9f5dfb6abec60b7af7474af61582'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
